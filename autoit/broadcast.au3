;
; Script for controlling an rFactor instance for broadcasting.
;

$serverTitle = "rFactor v1.255"

If $CmdLine[0] >= 1 Then
   $command = $CmdLine[1] ; chat, boot, ...
Else
   Exit 20 ; illegal number of arguments
EndIf



If WinActivate("[TITLE:" & $serverTitle & "]") Then
   If "keys" == $command Then
	  If $CmdLine[0] == 5 Then
		 Sleep($CmdLine[2])
		 AutoItSetOption("SendKeyDelay", $CmdLine[3])
		 AutoItSetOption("SendKeyDownDelay", $CmdLine[4])
		 Send($CmdLine[5])
	  Else
		 Exit 20 ; illegal number of arguments
	  EndIf
   ElseIf "talk" == $command Then
	  If $CmdLine[0] == 2 Then
		 $voice = ObjCreate("SAPI.SpVoice")
		 $voice.Speak($CmdLine[2])
	  Else
		 Exit 20 ; illegal number of arguments
	  EndIf
   EndIf
Else
   Exit 10 ; server not found
EndIf