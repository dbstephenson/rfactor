package net.rfactor.livescoring;

public interface LiveScoringConstants {
    public static final String TOPIC = "net/rfactor/livescoring/SCORINGDATA";
    public static final String SCORINGDATA = "scoringData";
    public static final String SCORINGSERVER = "scoringServer";
}
