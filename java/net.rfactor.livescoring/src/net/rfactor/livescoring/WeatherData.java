package net.rfactor.livescoring;

public class WeatherData {
	private final float m_darkCloud;
	private final float m_raining;
	private final float m_ambientTemp;
	private final float m_windX;
	private final float m_windY;
	private final float m_windZ;
	private final float m_onPathWetness;
	private final float m_offPathWetness;
	
	public WeatherData(float darkCloud, float raining, float ambientTemp, 
			float windX, float windY, float windZ, 
			float onPathWetness, float offPathWetness){
		m_darkCloud = darkCloud;
		m_raining = raining;
		m_ambientTemp = ambientTemp;
		m_windX = windX;
		m_windY = windY;
		m_windZ = windZ;
		m_onPathWetness = onPathWetness;
		m_offPathWetness = offPathWetness;
	}
	
	public float getDarkCloud() {
		return m_darkCloud;
	}
	
	public float getRaining() {
		return m_raining;
	}
	
	public float getAmbientTemp() {
		return m_ambientTemp;
	}
	
	public float getWindX() {
		return m_windX;
	}
	
	public float getWindY() {
		return m_windY;
	}
	
	public float getWindZ() {
		return m_windZ;
	}
	
	public float getOffPathWetness() {
		return m_offPathWetness;
	}
	
	public float getOnPathWetness() {
		return m_onPathWetness;
	}
}
