package net.rfactor.livescoring;

public class ScoringData {
    private final String m_trackName;
    private final int m_session;
    private final double m_lapDistance;
    private final double m_eventTime;
    private final int m_numberOfVehicles;
    private final VehicleData[] m_vehicleScoringData;
    private final String m_resultsStreamData;
	private final double m_endEventTime;

	private final int m_gamePhase;
	private final int m_yellowFlagState;
	private final int[] m_sectorFlags;
	private final int m_startLight;
	private final int m_numRedLights;
	
	private final WeatherData m_weatherData;
    
    public ScoringData(String trackName, int session, double lapDistance, double eventTime, double endEventTime, int gamePhase, int yellowFlagState, int[] sectorFlags, int startLight, int numRedLights, int numberOfVehicles, 
    		VehicleData[] vehicleScoringData, String resultsStreamData, WeatherData weatherData) {
        m_lapDistance = lapDistance;
        m_eventTime = eventTime;
		m_endEventTime = endEventTime;
		m_gamePhase = gamePhase;
		m_yellowFlagState = yellowFlagState;
		m_sectorFlags = sectorFlags;
		m_startLight = startLight;
		m_numRedLights = numRedLights;
        m_numberOfVehicles = numberOfVehicles;
        m_resultsStreamData = resultsStreamData;
        m_session = session;
        m_trackName = trackName;
        m_vehicleScoringData = vehicleScoringData;
        m_weatherData = weatherData;
    }

    public String getTrackName() {
        return m_trackName;
    }

    public int getSession() {
        return m_session;
    }

    public double getEventTime() {
        return m_eventTime;
    }

    public int getNumberOfVehicles() {
        return m_numberOfVehicles;
    }

    public VehicleData[] getVehicleScoringData() {
        return m_vehicleScoringData;
    }

    public String getResultsStreamData() {
        return m_resultsStreamData;
    }

    public double getLapDistance() {
        return m_lapDistance;
    }
    
	public double getEndEventTime() {
		return m_endEventTime;
	}

	public int getGamePhase() {
		return m_gamePhase;
	}

	public String getGamePhaseString() {
        // Game phases:
        // 0 Before session has begun
        // 1 Reconnaissance laps (race only)
        // 2 Grid walk-through (race only)
        // 3 Formation lap (race only)
        // 4 Starting-light countdown has begun (race only)
        // 5 Green flag
        // 6 Full course yellow / safety car
        // 7 Session stopped
        // 8 Session over
		switch (m_gamePhase) {
			case 0:
				return "Initializing";
			case 1:
				return "Reconnaissance";
			case 2:
				return "Grid walk-through";
			case 3:
				return "Formation lap";
			case 4:
				return "Starting-light countdown";
			case 5:
				return "Green flag";
			case 6:
				return "Safety car / Full course yellow";
			case 7:
				return "Session stopped";
			case 8:
				return "Session over";
		}
		return "Unknown";
	}

	public int getYellowFlagState() {
		return m_yellowFlagState;
	}

	public int[] getSectorFlags() {
		return m_sectorFlags;
	}

	public String getSectorFlagsString() {
		String result = "";
		for (int i = 0; i < m_sectorFlags.length; i++){
			result += (result != "" ? ", " : "") + "" + (i + 1) + ":" +
					(m_sectorFlags[i] > 0 ? (m_sectorFlags[i] == 7 ? "Red" : "Yellow") : "Green");
		}
		return result;
	}

	public int getStartLight() {
		return m_startLight;
	}

	public int getNumRedLights() {
		return m_numRedLights;
	}
	
	public WeatherData getWeatherData(){
		return m_weatherData;
	}
}
