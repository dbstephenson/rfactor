package net.rfactor.livescoring.client.ranking;

import java.util.Properties;

import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.livescoring.v2.impl.PluginListener;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(EventConstants.EVENT_TOPIC, LiveScoringConstants.TOPIC);
//        props.put(EventConstants.EVENT_FILTER, "(" + LiveScoringConstants.SCORINGSERVER + "=server-6789)");
        manager.add(createComponent()
            .setInterface(EventHandler.class.getName(), props)
            .setImplementation(RankingClient.class)
            .add(createServiceDependency()
                .setService(HttpService.class)
                .setRequired(false)
                .setAutoConfig(false)
                .setCallbacks("addHttpService", "removeHttpService")
                )
            );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
