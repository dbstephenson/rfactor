package net.rfactor.livescoring.client.endurance.impl;

import java.util.Properties;

import net.rfactor.chat.server.ChatService;
import net.rfactor.chat.server.KickService;
import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.livescoring.client.endurance.BackupService;
import net.rfactor.livescoring.client.endurance.EnduranceScoring;
import net.rfactor.serverlog.ServerLog;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(EventConstants.EVENT_TOPIC, LiveScoringConstants.TOPIC);
        props.put(Constants.SERVICE_PID, EnduranceScoring.SCORINGSERVICE_PID);
//        props.put(EventConstants.EVENT_FILTER, "(" + LiveScoringConstants.SCORINGSERVER + "=server-6789)");
        manager.add(createComponent()
            .setInterface(new String[] { EventHandler.class.getName(), EnduranceScoring.class.getName(), ManagedService.class.getName() }, props)
            .setImplementation(ScoringClient.class)
            .add(createServiceDependency()
                .setService(HttpService.class)
                .setRequired(false)
                .setAutoConfig(false)
                .setCallbacks("addHttpService", "removeHttpService")
            )
            .add(createServiceDependency()
        		.setService(BackupService.class)
        		.setRequired(false)
    		)
            .add(createServiceDependency()
                .setService(ServerLog.class)
                .setRequired(false)
            )
            .add(createServiceDependency()
                .setService(ChatService.class)
                .setRequired(false)
            )
            .add(createServiceDependency()
                .setService(KickService.class)
                .setRequired(false)
            )
        );
        
        manager.add(createComponent()
        	.setInterface(BackupService.class.getName(), null)
        	.setImplementation(BackupServiceImpl.class)
		);
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
