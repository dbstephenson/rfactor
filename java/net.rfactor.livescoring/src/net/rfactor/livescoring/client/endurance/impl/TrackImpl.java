package net.rfactor.livescoring.client.endurance.impl;

import net.rfactor.livescoring.client.endurance.Track;
import net.rfactor.livescoring.client.endurance.Vehicle;

public class TrackImpl implements Track {
    private RaceMode m_raceMode;
    private State m_state;
    private double m_lapDistance;
    private String m_name;
    private int m_laps;
    private double m_time;
    private double m_startTime;
    
    public TrackImpl(String name, int laps, double lapDistance) {
        m_name = name;
        m_laps = laps;
        m_lapDistance = lapDistance;
        m_state = State.FORMATING;
        m_raceMode = RaceMode.LAPS;
    }
    
    public TrackImpl(String name, double time, double lapDistance) {
        m_name = name;
        m_time = time;
        m_lapDistance = lapDistance;
        m_state = State.FORMATING;
        m_raceMode = RaceMode.TIME;
    }
    
    public TrackImpl(String name, int laps, double time, double lapDistance) {
        m_name = name;
        m_laps = laps;
        m_time = time;
        m_lapDistance = lapDistance;
        m_state = State.FORMATING;
        m_raceMode = RaceMode.LAPS_TIME;
    }
    
    @Override
	public double getLapDistance() {
        return m_lapDistance;
    }
    
    @Override
	public String getName() {
        return m_name;
    }
    
    @Override
	public int getLaps() {
        return m_laps;
    }
    
    @Override
    public double getTime() {
    	return m_time;
    }
    
    @Override
	public boolean isWinner(Vehicle vehicle) {
        switch (m_raceMode) {
            case LAPS:
                return vehicle.getLapsCompleted() >= m_laps;
            case TIME:
                return (vehicle.getRank() == 1 && vehicle.getLap() >= (m_startTime + m_time));
            case LAPS_TIME:
                return (vehicle.getLapsCompleted() >= m_laps) ||
                		(vehicle.getRank() == 1 && vehicle.getLap() >= (m_startTime + m_time));
        }
        return false;
    }

    @Override
	public void setState(State state) {
        m_state = state;
    }

    @Override
	public State getState() {
        return m_state;
    }

    @Override
	public void setStartTime(double et) {
        m_startTime = et;
    }
    
    @Override
	public double getStartTime() {
        return m_startTime;
    }
    
    @Override
	public RaceMode getRaceMode() {
        return m_raceMode;
    }
}
