package net.rfactor.livescoring.client.endurance.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;

import net.rfactor.livescoring.client.endurance.BackupService;
import net.rfactor.livescoring.client.endurance.Penalty;
import net.rfactor.livescoring.client.endurance.Vehicle;
import net.rfactor.util.TimeUtil;

public class BackupServiceImpl implements BackupService {
	private File m_backupDir;
	
	private void start() {
		m_backupDir = new File("backup");
		ensureBackupDirExists();
	}
	
	@Override
	public void backupStandings(SortedSet<Vehicle> standings, double eventTime, boolean useInternalScoring) {
		// In case of manual execution e.g. results_RACING_1-24-36.csv
		SortedSet<Vehicle> ranking = new TreeSet<Vehicle>(standings.comparator());
		ranking.addAll(standings);
		backupToFile(eventTime, useInternalScoring, ranking, "standings_");
	}

	@Override
	public void backupLapStandings(SortedSet<Vehicle> standings, double eventTime, boolean useInternalScoring) {
		// Default filename for backup e.g. scoring_1-24-36.csv
		SortedSet<Vehicle> ranking = new TreeSet<Vehicle>(new TrackPositionComparator());
		ranking.addAll(standings);
		backupToFile(eventTime, useInternalScoring, ranking, "lapstandings_");
	}
	
	@Override
	public void writeFinalStandings(SortedSet<Vehicle> standings) {
		StringBuffer sb = new StringBuffer();
		sb.append("<html><head><title>Final Standings</title></head><body>");
		sb.append("<table>"
			+ "<tr><th>Pos</th><th>PiC</th><th>Vehicle</th><th>Class</th><th>Drivers</th><th>Laps</th><th>Gap</th><th>Finish Time</th><th>Penalties</th></tr>");
		for (Vehicle v : standings) {
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(v.getRank());
			sb.append("</td><td>");
			sb.append(v.getPositionInClass());
			sb.append("</td><td>");
			sb.append(v.getVehicleName());
			sb.append("</td><td>");
			sb.append(v.getVehicleClass());
			sb.append("</td><td>");
			boolean first = true;
			for (String driver : v.getDrivers()) {
				sb.append((first ? "<b>" + driver + "</b>" : driver) + "<br />");
				first = false;
			}
			sb.append("</td><td>");
			sb.append(v.getLapsCompleted());
			sb.append("</td><td>");
			sb.append(v.getTimeBehindCarInFront());
			sb.append("</td><td>");
			sb.append(TimeUtil.toLapTime(v.getFinishedAt()));
            sb.append("</td><td>");
            for (Penalty p : v.getPenalties()) {
                sb.append((p.isResolved() ? "Resolved: " : "Unresolved: ") 
                    + p.getType() + " " 
                    + TimeUtil.toLapTime(p.getTime()) + " " + p.getReason() + " " + p.getDescription() + " "
                    + TimeUtil.toLapTime(p.getClearTime()) + " " + p.getClearReason() + " " + p.getClearUser() + "<br />"
                    );
            }
			sb.append("</td>");
			sb.append("</tr>");
		}
		sb.append("</body></html>");
		writeTextToFile(new File(m_backupDir, "standings_" + ".html"), sb.toString());
	}

	private void ensureBackupDirExists() {
		try {
		    File f = new File("backup");
			if (!f.exists()) {
				f.mkdir();
			}
		}
		catch (Exception e) {}
	}

	private void writeTextToFile(File logFileName, String message) {
		FileWriter fw = null;
		try {
		    // append to any existing file
		    fw = new FileWriter(logFileName);
		    fw.write(message);
		}
		catch (IOException e) {
		    // for some reason we could not write to the file
		    // and for now we just print that exception to the console
		    e.printStackTrace();
		}
		finally {
		    if (fw != null) {
		        try {
		            // and close the file, so we are sure it is flushed to disk
		            fw.close();
		        }
		        catch (IOException e) {
		        }
		    }
		}
	}

	private void backupToFile(double eventTime, boolean useInternalScoring, SortedSet<Vehicle> ranking, String standingsPrefix) {
		if (ranking != null && ranking.size() > 0) {
    		int lappos = 1;
    		String message = "Pos;ClassPos;Vehicle;LapsCompleted;TimeBehind;LapPos\n";
            for (Vehicle v : ranking) {
                String timeBehind = "---";
            	if (useInternalScoring) {
	            	if (v.getTimeBehindCarInFront() != -1.0) {
	            		timeBehind = TimeUtil.toLapTime(v.getTimeBehindCarInFront());
	            	}
            	}
            	else {
            		timeBehind = TimeUtil.toLapTime(v.getTimeBehindNext());
            	}
            	message += v.getRank() + ";" + v.getPositionInClass() + ";" + v.getVehicleName() + ";" + v.getLapsCompleted() + ";" + timeBehind + ";" + (lappos++) + "\n";
            }
            writeTextToFile(new File(m_backupDir, standingsPrefix + TimeUtil.toLapTime(eventTime,0).replace(":", "-").replace(".", "-") + ".csv"), message);
    	}
	}
}
