package net.rfactor.livescoring.client.endurance;

import java.util.List;
import java.util.Map;
import java.util.SortedSet;

public interface EnduranceScoring {
	public static final String SCORINGSERVICE_PID = "scoring";
	public static final String EVENTLAPS_KEY = "eventLaps";
	public static final String EVENTTIME_KEY = "eventTime";
	public static final String CLASSES_KEY = "classes";
	public static final String DRIVERS_KEY = "drivers";
    public static final String PITSPEED_KEY = "pitSpeed";
    public static final String INTERNALSCORING_KEY = "internalScoring";
    public static final String REJOINDELAY_KEY = "rejoinDelay";
    public static final String REJOINLAPTIME_KEY = "rejoinLaptime";

    /** Configures the event. */
//	public void configureEvent(int laps);
	
//	public void configureEvent(double seconds);
	
//	public void configureEvent(int laps, double seconds);
	
	/** Reset the event. */
	public void reset();
	
	/** Going green. */
	public void goingGreen();
	
	/** Stop the race */
	public void waveCheckeredFlag();
	
	public SortedSet<Vehicle> getRankedVehicles();

	public Track getTrack();
	
	public Penalty createPenalty(Vehicle vehicle, Penalty.Type type, String description, String reason, double time, double timeout);
	
	public double getEventTime();

	public List<String> getParticipatingClasses();

//	public void setParticipatingClasses(List<String> classesList);
	
	public int getLaps();
	
	public double getTime();

	public List<String> getParticipatingDrivers();

//	public void setParticipatingDrivers(List<String> driversList);

	public Map<String, Vehicle> getVehicles();

	public double getLastEt();
	
//	public void setPitlaneSpeedKmH(int pitlaneSpeedKmH);
	
	public int getPitlaneSpeedKmH();

	public boolean useInternalScoring();

    public Vehicle createVehicle(String vehicleName);
}
