package net.rfactor.livescoring.client.endurance;

import java.util.List;

import net.rfactor.livescoring.VehicleData;


public interface Vehicle {
	enum State {
		STARTING("Starting"), RACING("Racing"), FINISHED("Finished"), DISCONNECTED("Disconnected");

		private final String m_name;

		State(String name) {
			m_name = name;
		}

		public String getName() {
			return m_name;
		}
	}
	
	public int getID();

	public double getGarageZ();

	public double getGarageX();

	public boolean isGarageFound();

	public double getZ();

	public double getX();

	public int getLapsBehindNext();

	public double getTimeBehindNext();

	public int getLapsBehindLeader();

	public double getTimeBehindLeader();

	public double getSpeed();

	public boolean inPitlane();

	public List<DriverSwap> getDriverSwaps();

	public int getPositionInClass();

	public void setPositionInClass(int position);

	public String getVehicleName();

	public String getVehicleClass();

	public String getDriver();
	
	public List<String> getDrivers();

	public double getFinishedAt();

	public void setFinishedAt(double finishedAt);

	public double getCurrentLapDistance();

	public int getLapsCompleted();

	public void setLapsCompleted(int lapsCompleted);

	public double getLap();

	public double getSector2();

	public double getSector1();

	public int getEscOnTrack();

	public boolean isEscPressed();
	public void resetEscPressed();
	
	public double getEscPressedET();
	
	public boolean inGarage();

	public int getQualifyPosition();
	public void setQualifyPosition(int position);

	public State getState();

	public boolean isFinished();

	public boolean isRacing();

	public int getRank();

	public void setRank(int rank);
	
	public List<Penalty> getPenalties();

	public boolean setBlueFlag(Vehicle v, double lastEt);

	public boolean clearBlueFlag(Vehicle v, double lastEt);

	int getBlueFlags();

	public VehicleData getData();
	
	public double getET();

    public void setTimeBehindCarInFront(double delta);

    public void setTimeInFrontOfCarBehind(double delta);

    public double getTimeBehindCarInFront();

    public double getTimeInFrontOfCarBehind();

	public VehicleData getPreviousData();
	
	public double getPreviousET();
	
	public double distanceToOtherVehicle(Vehicle other);

	public void resetWhenOffline();
	
	public void setMessage(double et, String message);
	
	public List<VehicleMessage> getMessages(int lastXMessages);
	
    public int getPitstops();
    
    public boolean setPitstop(boolean inPit);
	
    public double getEscLapDistance();
    
    public boolean willPassSF(VehicleData data);
}
