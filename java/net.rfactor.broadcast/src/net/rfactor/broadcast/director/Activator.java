package net.rfactor.broadcast.director;

import java.util.Properties;

import net.rfactor.broadcast.controller.BroadcastService;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(EventConstants.EVENT_TOPIC, "serverlog/*");
//        props.put(Constants.SERVICE_PID, BroadcastService.SERVICE_PID);
        manager.add(createComponent()
            .setInterface(new String[] { EventHandler.class.getName() }, props)
            .setImplementation(DirectorImpl.class)
//            .add(createServiceDependency().setService(HttpService.class).setRequired(false).setCallbacks("add", "remove"))
        );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }
}
