package net.rfactor.broadcast.controller.impl;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rfactor.broadcast.controller.BroadcastService;
import net.rfactor.util.Util;

import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class BroadcastServiceImpl extends HttpServlet implements BroadcastService, Runnable, ManagedService {
    private static final String DEFAULT_SERVERNAME = "XS4ALL Endurance-Racing";
    private volatile BundleContext m_context;
    private File m_script;
    private volatile String m_server = DEFAULT_SERVERNAME;
    private volatile boolean m_pause = false;
    private Thread m_thread;
    private volatile boolean m_isRunning;
    private final PriorityBlockingQueue<Command> m_queue = new PriorityBlockingQueue<Command>();
//    private volatile ServerLog m_serverLog;
    
    public void start() {
        InputStream source = getClass().getResourceAsStream("/broadcast.exe");
        m_script = m_context.getDataFile("broadcast.exe");
        m_script.delete();
        FileOutputStream target = null;
        try {
            target = new FileOutputStream(m_script);
            Util.copyAndDigest(source, target);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
        	if (source != null) {
        		try {
					source.close();
				}
        		catch (IOException e) {
					e.printStackTrace();
				}
        	}
        	if (target != null) {
        		try {
					target.close();
				}
        		catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
        m_pause = false;
        m_isRunning = true;
        m_thread = new Thread(this, "Broadcast Thread");
        m_thread.start();
    }
    public void stop() {
        m_isRunning = false;
        m_thread.interrupt();
    }
    
    public void add(HttpService http) {
    	try {
			http.registerServlet("/broadcast", this, null, null);
		}
    	catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void remove(HttpService http) {
    	try {
    		http.unregister("/broadcast");
    	}
    	catch (IllegalArgumentException e) {
			e.printStackTrace();
    	}
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String subPath = req.getPathInfo();

		// depending on the path, this might be a request for a static resource, so thy that first
		if (subPath.startsWith("/css/") || subPath.startsWith("/js/") || subPath.startsWith("/images/") || subPath.endsWith(".html")) {
			if (subPath.toLowerCase().endsWith(".css")) {
				resp.setContentType("text/css");
			}
			if (subPath.toLowerCase().endsWith(".js")) {
				resp.setContentType("text/javascript");
			}
			if (subPath.toLowerCase().endsWith(".html")) {
				resp.setContentType("text/html; charset=utf-8");
			}
			if (subPath.toLowerCase().endsWith(".gif")) {
				resp.setContentType("image/gif");
			}
			BufferedInputStream bis = null;
			try {
				bis = new BufferedInputStream(this.getClass().getResourceAsStream(subPath));
				ServletOutputStream output = resp.getOutputStream();
				byte[] buffer = new byte[4096];
				int result = bis.read(buffer, 0, buffer.length);
				while (result != -1) {
					output.write(buffer, 0, result);
					result = bis.read(buffer, 0, buffer.length);
				}
				return;
			}
			finally {
				if (bis != null) {
					bis.close();
				}
			}
		}
		else if (subPath.startsWith("/talk")) {
			String msg = req.getParameter("msg");
			sendMessage(msg);
		}
		else if (subPath.startsWith("/keys")) {
			String keys = req.getParameter("keys");
			sendKeys(keys);
		}
		resp.getWriter().write("Ok");
    }
    
    public void sendMessage(String message) {
    	synchronized (m_queue) {
    		Command command = new Command(m_script.getAbsolutePath(), "talk", message);
    		execute(command);
//    		m_queue.put(command);
		}
    }
    
    public void sendKeys(String keys) {
    	synchronized (m_queue) {
    		Command command = new Command(m_script.getAbsolutePath(), "keys", "800", "20", "30", keys);
    		execute(command);
//    		m_queue.put(command);
		}
    }
    
    @Override
    public void setPause(boolean pause) {
    	m_pause = pause;
    }
    
    @Override
    public boolean getPause() {
    	return m_pause;
    }
    
    public void run() {
        while (m_isRunning) {
            try {
                Command command = m_queue.poll(5, TimeUnit.SECONDS);
                if (command != null) {
	                execute(command);
                }
            }
            catch (InterruptedException e) {
                // if we are interrupted, we should probably stop, so fall through here
            }
        }
    }
	private void execute(Command command) {
		try {
		    ProcessBuilder processBuilder = new ProcessBuilder();
		    processBuilder.directory(m_script.getParentFile()).command(command.getArgs());
		    processBuilder.redirectErrorStream(true);
		    Process process = processBuilder.start();
		    ProcessReader inputReader = new ProcessReader("Input reader for script process", process.getInputStream());
		    inputReader.start();
		    PrintWriter outputWriter = new PrintWriter(new OutputStreamWriter(process.getOutputStream()));
		    outputWriter.println();
		    outputWriter.flush();
//	            		m_serverLog.log(0, "Running: " + command);
		    int result = process.waitFor();
		    if (result > 0) {
//		            		m_serverLog.log(0, "Script returned error code " + result);
		    }
		    inputReader.stopThread();
		    outputWriter.close();
		    TimeUnit.MILLISECONDS.sleep(100);
		}
		catch (Exception e) {
		    // if for some reason we get an exception, we assume the process
		    // ended (we might get interrupted though, if someone does that, this
		    // might not be true)
		    e.printStackTrace();
		}
	}
    
    /**
     * For each process that gets started, two ProcessReader threads are started to relay
     * data from the process' stdout and error stream to our own reader.
     */
    private class ProcessReader extends Thread {
        private final InputStream m_input;
        private volatile boolean m_stopped = false;

        ProcessReader(String name, InputStream input) {
            super(name);
            m_input = input;
        }
        
        @Override
        public synchronized void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(m_input));
            String line = "";
            while (!m_stopped && line != null) {
                try {
                    line = reader.readLine();
                }
                catch (IOException e) {
                }
            }
        }
        
        /**
         * Makes this thread stop operating as soon as possible.
         */
        public void stopThread() {
            m_stopped = true;
        }
    }

    @Override
    public void updated(Dictionary properties) throws ConfigurationException {
        if (properties == null) {
            // no settings or existing settings deleted, revert to defaults
            m_server = DEFAULT_SERVERNAME;
        }
        else {
            // received settings, first validate them and then apply them
//            Object servername = properties.get(SERVERNAME_KEY);
//            if (servername instanceof String && !((String) servername).isEmpty()) {
//                m_server = (String) servername;
//            }
//            else {
//                throw new ConfigurationException(SERVERNAME_KEY, "Must be a non-empty string.");
//            }
        }
    }
}
