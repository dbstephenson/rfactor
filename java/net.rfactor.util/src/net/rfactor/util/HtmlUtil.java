package net.rfactor.util;

public class HtmlUtil {
    
    public static String PageHeader(String title){
    	return PageHeader(title, 0);
    }
    public static String PageHeader(String title, int refreshSeconds){
    	String header = "<html>\r\n" +
    				"<head>\r\n" +
					(refreshSeconds > 0 ? "<meta http-equiv=\"refresh\" content=\""+refreshSeconds+"\"/>\r\n" : "") +
					"<title>" + title + "</title>\r\n" +
					"</head>\r\n" + 
					"<style>" +
					"body, p, div, span, a, td, th { font:11px/1.60em Verdana,Tahoma,Arial,Helvetica,sans-serif;text-align:left;color:black; }\r\n" +
					"table { border: 0; background-color:#777; }\r\n" +
					"td { margin: 0px; padding: 2px; }\r\n" +
					"th { color: #fff; padding: 2px; background-color: #e40;}\r\n" +
					"tr { background-color: #eee; }\r\n" +
					"fieldset { border: 1px solid gray; padding: 3px 6px 6px 6px; border-radius: 5px; line-height:0.7em;}\r\n" +
					"legend { font-weight: bold; font-size: 11px; }" + 
					".left{ text-align:left; }\r\n" +
					".center{ text-align:center; }\r\n" +
					".right{ text-align:right; }\r\n" +
					".smallfont{  font-size:10px;line-height:1.1em; } " +
					".bigfont{  font-size:14px; } " +
					".dd{  background:url(/images/flag_green.png) no-repeat; } " +
					".blueFlagBox { height:16px;width:32px;background-color:blue;display:inline-block;margin-right:5px; }" +
					"</style>\r\n" +
					"</head>\r\n" +
					"<body>";
    	return header;
    }
    
    public static String PageFooter(){
    	String footer = "</body>\r\n" +
    			"</html>";
    	return footer;
    }

    public static String OverlayCSS() {
    	String css = "<style>" +
				"body { background-color:black; }\r\n" +
				"body, p, div, span, a, td, th { font:10px/1.30em Verdana,Tahoma,Arial,Helvetica,sans-serif;text-align:left;color:white; }\r\n" +
				"table { border: 0; background-color:#444; }\r\n" +
				"td { margin: 0px; padding: 1px 2px; }\r\n" +
				"th { color: #fff; padding: 1px 2px; background-color: black; }\r\n" +
				"tr { background-color: #000; }\r\n" +
				"fieldset { border: 1px solid gray; padding: 3px 6px 6px 6px; border-radius: 5px; line-height:0.7em;}\r\n" +
				"legend { font-weight: bold; font-size: 11px; }" + 
				".left{ text-align:left; }\r\n" +
				".center{ text-align:center; }\r\n" +
				".right{ text-align:right; }\r\n" +
				".smallfont{  font-size:10px;line-height:1.1em; } " +
				".bigfont{  font-size:14px; } " +
				"a { text-decoration:none }" +
				"a:hover { text-decoration:underline}" +
				"</style>\r\n";
    	return css;
    	
    }
}
