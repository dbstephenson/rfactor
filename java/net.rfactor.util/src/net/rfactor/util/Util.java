package net.rfactor.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Util {
    public static byte[] hashFile(File file) throws IOException {
        if ((file != null) && file.isFile()) {
                byte[] buffer = new byte[4096];
                MessageDigest digest;
                try {
                    digest = java.security.MessageDigest.getInstance("MD5");
                }
                catch (NoSuchAlgorithmException e) {
                    throw new IOException("No MD5 algorithm available", e);
                }
                FileInputStream fis = new FileInputStream(file);
                int i = fis.read(buffer);
                while (i != -1) {
                    digest.update(buffer, 0, i);
                    i = fis.read(buffer);
                }
                fis.close();
                return digest.digest();    
        }
        else {
            throw new IllegalArgumentException("Not a valid file: " + file);
        }
    }
    
    public static String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        if (bytes != null) {
            for (byte b : bytes) {
                String hex = Integer.toHexString(b);
                int length = hex.length();
                if (length > 2) {
                    hex = hex.substring(length - 2);
                }
                if (length < 2) {
                    sb.append('0');
                }
                sb.append(hex);
            }
        }
        return sb.toString();
    }
    
    public static byte[] toHash(String hash) {
        if (hash != null && hash.length() == 32) {
            byte[] code = new byte[16];
            for (int i = 0; i < code.length; i++) {
                code[i] = (byte) Integer.parseInt(hash.substring(i * 2, i * 2 + 2), 16);
            }
            return code;
        }
        else {
            System.err.println("Illegal hash: " + hash);
            return null;
        }
    }
    
    public static boolean hashesEqual(byte[] h1, byte[] h2) {
        if ((h1 == null) || (h2 == null) || (h1.length != 16) || (h2.length != 16)) {
            return false;
        }
        for (int i = 0; i < h1.length; i++) {
            if (h1[i] != h2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static void copyDir(File from, File to) throws IOException {
        if (!from.isDirectory()) {
            throw new IOException("Source is not a directory: " + from);
        }
        if (!to.isDirectory()) {
            throw new IOException("Target is not a directory: " + to);
        }
        for (File f : from.listFiles()) {
            File target = new File(to, f.getName());
            if (f.isFile()) {
                copy(new FileInputStream(f), new FileOutputStream(target));
            }
            if (f.isDirectory()) {
                target.mkdirs();
                copyDir(f, target);
            }
        }
    }

    public static void copy(InputStream source, OutputStream target) throws IOException {
        byte[] buffer = new byte[4096];
        int len = source.read(buffer);
        while (len != -1) {
            target.write(buffer, 0, len);
            len = source.read(buffer);
        }
    }
    
    public static byte[] copyAndDigest(InputStream source, OutputStream target) throws IOException {
        byte[] buffer = new byte[4096];
        MessageDigest digest;
        try {
            digest = java.security.MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new IOException("No MD5 algorithm available", e);
        }
        int len = source.read(buffer);
        while (len != -1) {
            digest.update(buffer, 0, len);
            target.write(buffer, 0, len);
            len = source.read(buffer);
        }
        return digest.digest();    
    }
}
