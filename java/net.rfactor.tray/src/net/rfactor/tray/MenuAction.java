package net.rfactor.tray;

public interface MenuAction {
    public void performAction();
    public String getName();
}
