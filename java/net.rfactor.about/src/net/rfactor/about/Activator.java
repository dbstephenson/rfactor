package net.rfactor.about;

import javax.swing.JOptionPane;

import net.rfactor.tray.MenuAction;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase implements MenuAction {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setInterface(MenuAction.class.getName(), null)
            .setImplementation(this)
        );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
    }

    public String getName() {
        return "About...";
    }

    public void performAction() {
        String app = System.getProperty("application.name", "Generic Application");
        JOptionPane.showMessageDialog(null, "<html><h1>" + app + "</h1>This application is licensed under the Apache License Version 2.0.");
    }

    public static void main(String[] args) {
        // just for quick testing
        (new Activator()).performAction();
        System.exit(0);
    }
}
