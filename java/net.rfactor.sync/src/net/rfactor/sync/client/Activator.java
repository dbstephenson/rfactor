package net.rfactor.sync.client;

import java.util.Properties;

import net.rfactor.sync.Index;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
	@Override
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		Properties props = new Properties();
		props.put(CommandProcessor.COMMAND_SCOPE, "syncclient");
		props.put(CommandProcessor.COMMAND_FUNCTION, new String[] {"reload", "download", "quit"});
		
		dm.add(createComponent()
			.setInterface(Object.class.getName(), props)
			.setImplementation(SyncClient.class)
			.add(createServiceDependency().setService(Index.class).setRequired(true))
            .add(createServiceDependency().setService(LogService.class).setRequired(false))
            .add(createServiceDependency().setService(LogReaderService.class).setRequired(false))
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {
	}
}
