package net.rfactor.sync.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rfactor.sync.Index;
import net.rfactor.sync.util.IOUtil;
import net.rfactor.sync.util.SyncConfig;
import net.rfactor.sync.util.SyncSourceConfig;

import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class SyncServlet extends HttpServlet {
	private volatile BundleContext m_context;
	private volatile Index m_index;
	private SyncSourceConfig m_source;
	private SyncConfig m_sync;
	private String m_path;
	private String m_sourcePath;
	
	public void start() {
		try {
			Properties p = new Properties();
			File serverProperties = new File("server.properties");
			if (!serverProperties.isFile()) {
				throw new IllegalArgumentException("Could not locate server configuration file: " + serverProperties.getAbsolutePath());
			}
			p.load(new FileInputStream(serverProperties));
			String path = p.getProperty("path");
			String source = p.getProperty("source");
			if (path != null && source != null) {
				if (!(new File(path)).isDirectory()) {
					throw new IllegalArgumentException("The 'path' configured in the properties is not a directory: " + path);
				}
				if (!(new File(source)).isFile()) {
					throw new IllegalArgumentException("The 'source' configured in the properties is not a file: " + path);
				}
				m_path = path;
				m_sourcePath = source;
			}
			reload();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reload() throws Exception {
		if (m_sourcePath != null && m_path != null) {
			System.out.println("Reloading server state...");
			m_source = SyncSourceConfig.create(new FileInputStream(new File(m_sourcePath)));
			File config = new File(m_context.getDataFile(""), IOUtil.calculateSHA(new FileInputStream(new File(m_sourcePath))) + ".index");
			m_sync = SyncConfig.scan(new File(m_path), m_source, config);
			m_sync.getIndexer().save(new FileOutputStream(config));
			System.out.println("Done reloading server state.");

		}
	}
	
	public void quit() throws BundleException {
		m_context.getBundle(0).stop();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = req.getPathInfo();
		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		System.out.println("GET " + path);
		String paths[] = path.split("/");
		if (paths.length > 0) {
			if ("hash".equals(paths[0])) {
				if (paths.length > 1) {
					String hash = paths[1];
					System.out.println("Looking up hash " + hash);
					File file = m_index.lookup(hash);
					if (file == null) {
						file = m_sync.getIndexer().lookup(hash);
						if (file == null) {
							resp.sendError(404, "File not found for hash " + hash);
							return;
						}
					}
					resp.setContentType("application/octet-stream");
					ServletOutputStream os = resp.getOutputStream();
					IOUtil.copy(new FileInputStream(file), os);
				}
				else {
					resp.setContentType("text/plain");
					PrintWriter writer = resp.getWriter();
					for (String hash : m_index.hashes()) {
						writer.write(hash);
						writer.write('\n');
					}
					return;
				}
			}
			else if ("config".equals(paths[0])) {
				if (m_sync != null) {
					m_sync.write(resp.getOutputStream());
				}
			}
			else if ("status".equals(paths[0])) {
				resp.setHeader("Access-Control-Allow-Origin", "*");
				resp.setContentType("text/plain");
				PrintWriter writer = resp.getWriter();
				writer.write("Online");
				return;
			}
		}
	}
	
	private void add(HttpService service) {
		try {
			service.registerServlet("/sync", this, null, null);
		}
		catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NamespaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void remove(HttpService service) {
		service.unregister("/sync");
	}
}
