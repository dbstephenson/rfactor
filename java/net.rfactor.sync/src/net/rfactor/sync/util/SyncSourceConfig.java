package net.rfactor.sync.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Configuration for a synchronization source.
 */
public class SyncSourceConfig {
	private final List<Pattern> m_ignores = new ArrayList<Pattern>();
	private final List<Pattern> m_includes = new ArrayList<Pattern>();
	
	private SyncSourceConfig() {
	}
	
	public static SyncSourceConfig create(InputStream is) throws IOException {
		try {
			SyncSourceConfig ssc = new SyncSourceConfig();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = br.readLine();
			Pattern pattern = Pattern.compile("(.*) : (include|ignore)");
			while (line != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches()) {
					String g1 = matcher.group(1);
					String g2 = matcher.group(2);
					if ("include".equals(g2)) {
						ssc.addInclude(Pattern.compile(g1));
					}
					else if ("ignore".equals(g2)) {
						ssc.addIgnore(Pattern.compile(g1));
						
					}
				}
				line = br.readLine();
			}
			return ssc;
		}
		finally {
			is.close();
		}
	}
	
	public List<Pattern> getIgnores() {
		return m_ignores;
	}
	
	public void addInclude(Pattern include) {
		m_includes.add(include);
	}
	
	public void addIgnore(Pattern ignore) {
		m_ignores.add(ignore);
	}
	
	public boolean include(String file) {
		boolean include = true;
		for (Pattern p : m_ignores) {
			if (p.matcher(file).matches()) {
				include = false;
				break;
			}
		}
		if (!include) {
			for (Pattern p : m_includes) {
				if (p.matcher(file).matches()) {
					include = true;
					break;
				}
			}
		}
		return include;
	}
}