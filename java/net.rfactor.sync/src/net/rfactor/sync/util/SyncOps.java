package net.rfactor.sync.util;

import java.util.ArrayList;
import java.util.List;

public class SyncOps {
	private List<HashedFile> m_downloads = new ArrayList<HashedFile>();
	private List<HashedFile> m_delete = new ArrayList<HashedFile>();
	private Indexer m_index;
	
	public void setDownloads(List<HashedFile> list) {
		m_downloads.clear();
		m_downloads.addAll(list);
	}
	
	public List<HashedFile> getDownloadFiles() {
		return new ArrayList<HashedFile>(m_downloads);
	}

	public List<HashedFile> getDeleteFiles() {
		return new ArrayList<HashedFile>(m_delete);
	}

	public void delete(String file, String sha) {
		m_delete.add(new HashedFile(file, sha));
	}
	
	public void found(HashedFile sf) {
		m_downloads.remove(sf);
	}

	public void setIndex(Indexer index) {
		m_index = index;
	}
	
	public Indexer getIndex() {
		return m_index;
	}
}
