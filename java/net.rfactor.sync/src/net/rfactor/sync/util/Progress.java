package net.rfactor.sync.util;

public class Progress {
    private double m_progress;
    private String m_task;
    
    public Progress(String task) {
        m_task = task;
    }

    public synchronized void setProgress(double percentage) {
        m_progress = percentage;
    }

    public synchronized double getProgress() {
        return m_progress;
    }

    public String getTask() {
        return m_task;
    }
}
