﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;

namespace rF_TVDirector
{
    public class TVDirector
    {
        #region VARIABLES
        /// <summary>
        /// Position Number of the own car
        /// </summary>
        private int _OwnPosition = 0;

        /// <summary>
        /// Position Number of the car to switch to
        /// </summary>
        private int _NewPosition = 0;

        /// <summary>
        /// Save the time when the position has been changed
        /// </summary>
        private DateTime _PositionTimer = new DateTime(0);

        /// <summary>
        /// Name of the game to activate
        /// </summary>
        private readonly string _GameName = "rfactor";

        /// <summary>
        /// Time in milliseconds between Sendkey execution
        /// </summary>
        private int _TimeBetweenSendKey = 30;

        /// <summary>
        /// Time in milliseconds to wait after activation of the game window
        /// </summary>
        private int _TimeAfterWindowActivation = 100;

        private string LastError = "";
        #endregion VARIABLES

        #region PROPERTIES
        public int TimeBetweenSendKey
        {
            get { return _TimeBetweenSendKey; }
            set { _TimeBetweenSendKey = value; }
        }
        public int TimeAfterWindowActivation
        {
            get { return _TimeAfterWindowActivation; }
            set { _TimeAfterWindowActivation = value; }
        }
        public int OwnPosition
        {
            get { return _OwnPosition; }
            set { _OwnPosition = (value >= 0 ? value : 0); }
        }

        public int NewPosition
        {
            get { return _NewPosition; }
            set {
                if (_NewPosition != (value >= 0 ? value : 0))
                {
                    _PositionTimer = DateTime.Now;
                }
                _NewPosition = (value >= 0 ? value : 0); 
            }
        }

        public double PositionSince
        {
            get { return (DateTime.Now - _PositionTimer).TotalSeconds; }
        }
        #endregion PROPERTIES

        #region CONSTRUCTOR / DESTRUCTOR
        public TVDirector()
        {
            _OwnPosition = 0;
        }
        #endregion CONSTRUCTOR / DESTRUCTOR

        #region THREADING
        #endregion THREADING

        #region PUBLIC METHODS

        /// <summary>
        /// Show own position (car)
        /// </summary>
        public void ResetPosition()
        {
            SwitchToPosition(-1);
        }

        /// <summary>
        /// Switch camera to the new position (and set own position)
        /// </summary>
        /// <param name="CarPosition">Position to show</param>
        /// <param name="ownPosition">Own position</param>
        internal string SwitchToPosition(int CarPosition, int ownPosition)
        {
            OwnPosition = ownPosition;
            return SwitchToPosition(CarPosition);
        }
        /// <summary>
        /// Switch camera to the new position
        /// </summary>
        /// <param name="CarPosition">Position to show</param>
        internal string SwitchToPosition(int CarPosition)
        {
            NewPosition = CarPosition;
            // Make sure that the gamewindow is active
            if (!ActivateGame()) return LastError;

            // Wait a sort while to let the window get active
            Thread.Sleep(_TimeAfterWindowActivation);

            // First jump to own car
            SendKey(WindowsInput.VirtualKeyCode.EXECUTE, WindowsInput.DirectXKeyCode.DIK_NUMPADENTER);

            // If the position is valid, loop thru the field until the correct position is shown
            if (NewPosition > 0)
            {

                if (NewPosition > _OwnPosition)
                {
                    // New position is behind own position
                    for (int i = _OwnPosition; i < NewPosition; i++)
                    {
                        Thread.Sleep(_TimeBetweenSendKey);
                        SendKey(WindowsInput.VirtualKeyCode.SUBTRACT, WindowsInput.DirectXKeyCode.DIK_SUBTRACT);
                    }
                }
                else if (NewPosition < _OwnPosition)
                {
                    // New position is in front of our position
                    for (int i = NewPosition; i < _OwnPosition; i++)
                    {
                        Thread.Sleep(_TimeBetweenSendKey);
                        SendKey(WindowsInput.VirtualKeyCode.ADD, WindowsInput.DirectXKeyCode.DIK_ADD);
                    }
                }
            }
            return "";
        }
        #endregion PUBLIC METHODS

        #region PRIVATE METHODS
        /// <summary>
        /// Activate the gamewindow so that it can receive the keyboard entries
        /// </summary>
        private bool ActivateGame()
        {
            LastError = "";
            Process[] processes = Process.GetProcessesByName(_GameName);
            IntPtr ptWindow;
            if (processes.Length > 0)
            {
                ptWindow = processes[0].MainWindowHandle;
                if (ptWindow != IntPtr.Zero)
                {
                    Win32.ShowWindow(ptWindow, Win32.SW_RESTORE);
                    Win32.SetForegroundWindow(ptWindow);
                    int iCnt = 0;
                    while (iCnt < 10)
                    {
                        iCnt++;
                        Thread.Sleep(100);
                        IntPtr activeWindowHandle = Win32.GetForegroundWindow();
                        if (activeWindowHandle == ptWindow)
                        {
                            return true;
                        }
                        Win32.ShowWindow(ptWindow, Win32.SW_RESTORE);
                        Win32.SetForegroundWindow(ptWindow);
                    }
                    LastError = "Unable to bring the gamewindow to the foreground";
                }
                else
                {
                    LastError = "No Window Handle";
                }
            }
            else
            {
                LastError = "rFactor not running";
            }
            return false; ;
        }

        private static void SendKey(WindowsInput.VirtualKeyCode vKey, WindowsInput.DirectXKeyCode xKey)
        {
            Win32.keybd_event((byte)vKey, (byte)xKey, Win32.KEYEVENTF_SCANCODE, UIntPtr.Zero);
            Thread.Sleep(30);
            Win32.keybd_event((byte)vKey, (byte)xKey, Win32.KEYEVENTF_SCANCODE | Win32.KEYEVENTF_KEYUP, UIntPtr.Zero);
        }

        #endregion PRIVATE METHODS
    }
}
